// Package notepad provides a simple CRUD application in a web page.
package notepad

import (
	"net/http"

	"github.com/blue-jay/blueprint/lib/flight"
	"github.com/blue-jay/blueprint/middleware/acl"
	"github.com/blue-jay/blueprint/model"
	"github.com/blue-jay/blueprint/model/note"
	"github.com/blue-jay/core/router"
)

var (
	uri = "/grade"
)

// Load the routes.
func Load() {
	c := router.Chain(acl.DisallowAnon)
	router.Get(uri, Index, c...)
	router.Post(uri, Search, c...)
	router.Get(uri+"/create", Create, c...)
	router.Post(uri+"/create", Store, c...)
	router.Get(uri+"/view/:id", Show, c...)
	router.Get(uri+"/edit/:id", Edit, c...)
	router.Patch(uri+"/edit/:id", Update, c...)
	router.Delete(uri+"/:id", Destroy, c...)
}

// Search displays the items.
func Search(w http.ResponseWriter, r *http.Request) {
	c := flight.Context(w, r)
	query := r.FormValue("subject")

	if query == "" {
		Index(w, r)
		return
	}

	grades, _, err := model.Note.BySubject(query, c.UserID)
	if err != nil {
		c.FlashError(err)
		grades = []note.Grade{}
	}

	v := c.View.New("note/index")
	v.Vars["grades"] = grades
	v.Render(w, r)
}

// Index displays the items.
func Index(w http.ResponseWriter, r *http.Request) {
	c := flight.Context(w, r)

	grades, _, err := model.Note.ByUserID(c.UserID)

	if err != nil {
		c.FlashError(err)
		grades = []note.Grade{}
	}

	v := c.View.New("note/index")
	v.Vars["grades"] = grades
	v.Render(w, r)
}

// Create displays the create form.
func Create(w http.ResponseWriter, r *http.Request) {
	c := flight.Context(w, r)

	v := c.View.New("note/create")
	c.Repopulate(v.Vars, "name", "grade")
	v.Render(w, r)
}

// Store handles the create form submission.
func Store(w http.ResponseWriter, r *http.Request) {
	c := flight.Context(w, r)

	if !c.FormValid("name", "grade") {
		Create(w, r)
		return
	}
	_, err := model.Note.Create(r.FormValue("name"), r.FormValue("grade"), c.UserID)
	if err != nil {
		c.FlashError(err)
		Create(w, r)
		return
	}

	c.FlashSuccess("Note hinzugefügt!")
	c.Redirect(uri)
}

// Show displays a single item.
func Show(w http.ResponseWriter, r *http.Request) {
	c := flight.Context(w, r)

	item, _, err := model.Note.ByID(c.Param("id"), c.UserID)
	if err != nil {
		c.FlashError(err)
		c.Redirect(uri)
		return
	}

	v := c.View.New("note/show")
	v.Vars["item"] = item
	v.Render(w, r)
}

// Edit displays the edit form.
func Edit(w http.ResponseWriter, r *http.Request) {
	c := flight.Context(w, r)

	item, _, err := model.Note.ByID(c.Param("id"), c.UserID)
	if err != nil {
		c.FlashError(err)
		c.Redirect(uri)
		return
	}

	v := c.View.New("note/edit")
	c.Repopulate(v.Vars, "name", "grade")
	v.Vars["item"] = item
	v.Render(w, r)
}

// Update handles the edit form submission.
func Update(w http.ResponseWriter, r *http.Request) {
	c := flight.Context(w, r)

	if !c.FormValid("name", "grade") {
		Edit(w, r)
		return
	}

	_, err := model.Note.Update(r.FormValue("name"), r.FormValue("grade"), c.Param("id"), c.UserID)
	if err != nil {
		c.FlashError(err)
		Edit(w, r)
		return
	}

	c.FlashSuccess("Note updated.")
	c.Redirect(uri)
}

// Destroy handles the delete form submission.
func Destroy(w http.ResponseWriter, r *http.Request) {
	c := flight.Context(w, r)

	_, err := model.Note.DeleteSoft(c.Param("id"), c.UserID)
	if err != nil {
		c.FlashError(err)
	} else {
		c.FlashNotice("Note gelöscht.")
	}

	c.Redirect(uri)
}
