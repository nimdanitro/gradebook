// Package note provides access to the note table in the MySQL database.
package note

import (
	"database/sql"
	"fmt"

	"github.com/go-sql-driver/mysql"
)

var (
	// table is the table name.
	table = "note"
)

// Item defines the model.
type Item struct {
	ID        uint32         `db:"id"`
	Name      string         `db:"name"`
	Grade     float32        `db:"grade"`
	UserID    uint32         `db:"user_id"`
	CreatedAt mysql.NullTime `db:"created_at"`
	UpdatedAt mysql.NullTime `db:"updated_at"`
	DeletedAt mysql.NullTime `db:"deleted_at"`
}

// UserInfo defines the information of a user
type UserInfo struct {
	FirstName string `db:"first_name"`
	LastName  string `db:"last_name"`
}

// Grade defines the grade object
type Grade struct {
	Item
	UserInfo
}

// Service defines the database connection.
type Service struct {
	DB Connection
}

// Connection is an interface for making queries.
type Connection interface {
	Exec(query string, args ...interface{}) (sql.Result, error)
	Get(dest interface{}, query string, args ...interface{}) error
	Select(dest interface{}, query string, args ...interface{}) error
}

// ByID gets an item by ID.
func (s Service) ByID(ID string, userID string) (Item, bool, error) {
	result := Item{}
	err := s.DB.Get(&result, fmt.Sprintf(`
		SELECT id, name, grade, user_id, created_at, updated_at, deleted_at
		FROM %v
		WHERE id = ?
			AND user_id = ?
			AND deleted_at IS NULL
		LIMIT 1
		`, table),
		ID, userID)
	return result, err == sql.ErrNoRows, err
}

// BySubject gets an item by ID.
func (s Service) BySubject(subject string, userID string) ([]Grade, bool, error) {
	var result []Grade
	err := s.DB.Select(&result, fmt.Sprintf(`
		SELECT n.id, n.name, n.grade, n.user_id, n.created_at, n.updated_at, n.deleted_at, u.first_name, u.last_name
		FROM %v n
		JOIN user u on u.id = n.user_id
		WHERE n.user_id = ? AND n.deleted_at IS NULL AND n.name LIKE '%s'
		`, table, subject),
		userID)
	return result, err == sql.ErrNoRows, err
}

// ByUserID gets all items for a user.
func (s Service) ByUserID(userID string) ([]Grade, bool, error) {
	var result []Grade
	err := s.DB.Select(&result, fmt.Sprintf(`
		SELECT n.id, n.name, n.grade, n.user_id, n.created_at, n.updated_at, n.deleted_at, u.first_name, u.last_name
		FROM %v n
		JOIN user u on u.id = n.user_id
		WHERE n.user_id = ?
			AND n.deleted_at IS NULL
		`, table),
		userID)
	return result, err == sql.ErrNoRows, err
}

// Create adds an item.
func (s Service) Create(name string, grade string, userID string) (sql.Result, error) {
	result, err := s.DB.Exec(fmt.Sprintf(`
		INSERT INTO %v
		(name, grade, user_id)
		VALUES
		(?,?,?)
		`, table),
		name, grade, userID)
	return result, err
}

// Update makes changes to an existing item.
func (s Service) Update(name string, grade string, ID string, userID string) (sql.Result, error) {
	result, err := s.DB.Exec(fmt.Sprintf(`
		UPDATE %v
		SET name = ?, grade = ?
		WHERE id = ?
			AND user_id = ?
			AND deleted_at IS NULL
		LIMIT 1
		`, table),
		name, grade, ID, userID)
	return result, err
}

// DeleteHard removes an item.
func (s Service) DeleteHard(ID string, userID string) (sql.Result, error) {
	result, err := s.DB.Exec(fmt.Sprintf(`
		DELETE FROM %v
		WHERE id = ?
			AND user_id = ?
			AND deleted_at IS NULL
		`, table),
		ID, userID)
	return result, err
}

// DeleteSoft marks an item as removed.
func (s Service) DeleteSoft(ID string, userID string) (sql.Result, error) {
	result, err := s.DB.Exec(fmt.Sprintf(`
		UPDATE %v
		SET deleted_at = NOW()
		WHERE id = ?
			AND user_id = ?
			AND deleted_at IS NULL
		LIMIT 1
		`, table),
		ID, userID)
	return result, err
}
